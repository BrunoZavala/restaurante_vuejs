import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist';
Vue.use(Vuex)

const vuexPersist = new VuexPersist({
  key: "my-app",
  storage: localStorage
});

export default new Vuex.Store({
  state:{
    user: {
      logged: 0,
      data: ''
    },
  },
  mutations:{
    loginuser(state, data) {
      state.user.data = data;
      state.user.logged = 1;
    },
    logoutuser(state) {
      state.user.data = '';
      state.user.logged = 0;
    },
    toggleSidebarDesktop (state) {
      const sidebarOpened = [true, 'responsive'].includes(state.sidebarShow)
      state.sidebarShow = sidebarOpened ? false : 'responsive'
    },
    toggleSidebarMobile (state) {
      const sidebarClosed = [false, 'responsive'].includes(state.sidebarShow)
      state.sidebarShow = sidebarClosed ? true : 'responsive'
    },
    set (state, [variable, value]) {
      state[variable] = value
    }
  },
  getters:{
    userData: state => {
      return state.user.data.user
    },
    userToken: state => {
      return state.user.data.token
    },
  },
  plugins: [vuexPersist.plugin]
})