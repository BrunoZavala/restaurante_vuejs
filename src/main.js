import 'core-js/stable'
import Vue from 'vue'
import App from './App'
import router from '@/router'
import CoreuiVue from '@coreui/vue'
import { iconsSet as icons } from './assets/icons/icons.js'
import store from './store'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Snotify, { SnotifyPosition } from 'vue-snotify'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

const moment = require('moment');
require('moment/locale/es');

Vue.config.performance = true
Vue.use(CoreuiVue)
Vue.prototype.$log = console.log.bind(console)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(Snotify, options);
Vue.use(require('vue-moment'), {
  moment
});

const options = {
  toast: {
    position: SnotifyPosition.centerTop
  }
};
new Vue({
  el: '#app',
  router,
  store,
  icons,
  template: '<App/>',
  components: {
    App
  }
})
